Title: Stax is going open source
Date: 2021-03-01
Category: News
Tags: news, open source
Slug: stax-open-source
Author: Félix David
Lang: en

After a long journey as an in-house review tool at Superprod Studio, Stax takes off and flies into the world of Open Source!

### Wonderful! But what's Stax?

It's a review toolset based on Blender's [*VSE*](https://docs.blender.org/manual/en/latest/video_editing/sequencer/index.html).

After testing a lot of review tools, without finding one that perfectly fits our needs, we decided to create our own review toolset: Stax.

![Stax](images/StaxLoaded.png)

### Main features

- Review session
    - Blender's *VSE* (editing module) allows the users to review a whole timeline. No more reviewing one media at a time.
    - Every media is viewed in its own editing context, with its previous versions, and the previous and next steps.
    - And you can, as simple as clicking on a play button, watch your whole editing with all your notes and your colleagues' ones.

- Inter-media comparison, to compare two versions for example.
    - Transparency
    - Blending mode (Multiply, Difference...)
    - Wipe
    - Side-by-Side

- Notes creation/edition
    - Thanks to Blender's [*Grease Pencil*](https://docs.blender.org/manual/en/latest/grease_pencil/index.html), you can easily add visual notes, modify them, duplicate them...
    - Stax also bundles a textual notes module to write complex descriptions.
    - And of course the notes may have a frame range.

- Notes publishing
    - If you work with a Production Tracker (like Kitsu, Shotgun...), you can link Stax to it, to publish your reviews and make them available to all your teams.

And many more features!

### Open sourcing Stax

First of all because we have a taste for sharing.
Also because Stax has raised great interest from several studios, and if it can satisfy all review's needs, a pooling is mandatory for its growth.

### We are not alone

Stax is mainly supported by Superprod Studio. 
Ubisoft, The Yard and Malil'Art already contribute to its development.

### What about Stax's future?

- New features regarding new needs
- Improved flexibility to ease its integration in different workflows
- Some features may be split into independent modules so they may be used out of the common review scope

### Would you get on board?

- Join our [Discord Channel](https://discord.gg/8ZNx7Vz) 
- Explore the [Documentation](https://superprod.gitlab.io/stax_suite/stax/docs/)
- Read the [Contributing](https://gitlab.com/superprod/stax_suite/stax/-/blob/master/CONTRIBUTING.md) Doc
- Rummage our [GitLab](https://gitlab.com/superprod/stax_suite/stax/)

Currently Stax is provided as an [Application Template](https://docs.blender.org/manual/en/latest/advanced/app_templates.html).

### A video for thousand words

To picture Stax's current state and the needs it fits, you can watch our presentation at [RADI 2021](https://youtu.be/MY2fEUNfJKE?t=1010), in french.

### Why does Stax is called like that?

Because of the media stacking feature, which is one of the main ones, and as an hommage to the music label [Stax Records](https://staxrecords.com/).

### Tell me more!

- Stax uses [OpenTimelineIO](https://opentimelineio.readthedocs.io/en/latest/) to load the timelines
- We created [OpenReviewIO](https://pypi.org/project/openreviewio/), to ease review information exchange between softwares
- All of it is in Python 3