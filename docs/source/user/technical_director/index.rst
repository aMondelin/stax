===================
Technical Director
===================
To set a user's session up, you can automatically start Stax targetting the timeline you want and link directory containing existing reviews.

.. highlight:: python

Load a timeline
===============
:ref:`Load <load timeline>` all media from an ``.otio`` file.


.. code-block:: python
   
   import bpy

   bpy.ops.sequencer.load_timeline(
      filepath="/path/test.otio",
   )


Handle reviews
==============
.. include:: /features/link_reviews.rst
   :start-after: Link Reviews
   :end-before: To

The python operator to :ref:`link <link reviews>` the target directory for reviews is:

.. code-block:: python

   import bpy

   bpy.ops.scene.link_reviews(
      directory="/path/OpenReviewIO_directory"
   )

New session's setup
--------------------
You may want to keep control over the directory where the users will read or write their reviews. If the new session doesn't have any existing reviews you still can target an empty directory to be used when :ref:`publishing <publish reviews>`.
All the edited reviews during the session will be automatically written to the directory without asking the user to choose one.


Share reviews
-------------
To share reviews between users, you must target the same directory (on a server for example) in every open Stax's session. Doing so, updating will synchronize all the existing reviews.

.. note:: 

   Stax names the reviews ``.orio`` folders using ``media_name.ext.orio`` which is a convention from OpenReviewIO. But if you have medias with the same name, but under a different path,
   you can set an unique name to your ``.orio`` folder: ``media_name_<uuid>.ext.orio`` for example.
   Everything will still be linked as expected.

.. important::

   You must load the timeline **before** linking the reviews.

Launch
======
The python script must be executed through Blender. 
You can write your script into a ``.py`` file and launch Blender with this file using a command.

.. _`text Editor`: https://docs.blender.org/manual/en/latest/editors/text_editor.html

.. _`command line`:

Command Line
------------
.. highlight:: bash

Raw command:

.. code-block:: bash

   /path/to/blender --app-template stax --python-expr "import bpy;bpy.ops.sequencer.load_timeline(filepath='/path/test.otio')"


Target a script containing the python code:

.. code-block:: bash

   /path/to/blender --app-template stax --python path/to/start_stax.py
   


What if I find a bug
====================

**No way it happens !**

But if that happens, first follow these steps :

-  Delete your Stax config file ``cfg.json`` in
   ``C:\Users\UserName\AppData\Roaming\Stax``
-  Do a :ref:`full-update` of your timeline.

If there’s still a problem, ask for help on the discord_ ``#dev`` channel.

.. _discord: https://discord.gg/fceFNFH
