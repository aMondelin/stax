Callbacks
=========

.. warning::

   We strongly recomment you to **NOT** use callbacks for durability as they are meant to be removed in future.


Callbacks allow to build dedicated processes when some events are executed in Stax.

To implement custom callbacks, it's necessary to create a class inheriting from :class:`stax.Callbacks_Script.Callbacks`. 
Unlike `Override Classes`, it is not necessary to ``(un)register()`` this class.

It's possible to implement only the needed methods and ignore the others.

List
----
.. autofunction:: stax.Callbacks_Script.Callbacks.authentication_exec

.. autofunction:: stax.Callbacks_Script.Callbacks.session_update_pre

.. autofunction:: stax.Callbacks_Script.Callbacks.review_session_publish

Example
-------
.. highlight:: python
.. code-block:: python

    import stax

    class MyCallbacks(stax.Callbacks):
        name = "Custom Callbacks"
        description = "Interact with my data"

        def review_session_publish(edited_notes):
            for review, note in edited_notes:
                ...