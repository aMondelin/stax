import pytest
from addon_helper import get_version
import importlib
from pathlib import Path

import bpy

import opentimelineio as otio
import openreviewio as orio

from stax.utils.utils_timeline import get_parent_metas


def test_toggle_versions():
    """Toggle versions"""

    source_timeline_path = Path(__file__).parent.joinpath("data", "timeline_test.otio")
    source_timeline = otio.adapters.read_from_file(str(source_timeline_path))
    bpy.ops.sequencer.load_timeline(filepath=str(source_timeline_path))

    scene = bpy.context.scene
    sequence_editor = scene.sequence_editor

    sequences_start_count = len(sequence_editor.sequences)

    # Toggle the sequence with more than 2 versions
    sequence = None
    sequence_element = None
    for track in scene.tracks:
        for element in track.elements:
            if len(element.versions) > 2:
                media_sequence = sequence_editor.sequences_all.get(
                    element.versions[-1].name
                )
                sequence = get_parent_metas(media_sequence)[0]
                sequence_element = element
                break

    # Set the frame
    scene.frame_set(sequence.frame_start)

    # Set the track
    scene.current_displayed_track = scene.tracks[1].name

    # Toggle versions ON
    # ===================
    bpy.ops.sequencer.toggle_versions()

    assert (
        len(sequence_editor.sequences)
        == sequences_start_count + len(sequence_element.versions) - 1
    )

    # Toggle versions OFF
    # ===================
    bpy.ops.sequencer.toggle_versions()

    # Test tracks are correctly back to normal
    assert {track.name for track in source_timeline.tracks} == {
        track.name for track in scene.tracks
    }

    # Test there is no remaining sequence
    assert len(sequence_editor.sequences) == sequences_start_count


def test_show_annotations():
    """Show annotations"""

    source_timeline_path = Path(__file__).parent.joinpath("data", "timeline_test.otio")
    source_timeline = otio.adapters.read_from_file(str(source_timeline_path))

    orio_dir = Path(__file__).parent.joinpath("data", "OpenReviewIO")

    # Link reviews
    bpy.ops.scene.link_reviews(directory=str(orio_dir))
