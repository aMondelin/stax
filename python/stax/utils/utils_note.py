# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software Foundation,
#  Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# ##### END GPL LICENSE BLOCK #####
"""
All functions to perform note actions shared by several operators
"""

import bpy
from bpy.types import Sequence

from .utils_timeline import get_parent_metas


def clear_advanced_drawings(sequence: Sequence):
    """Clear advanced drawings of a sequence.

    :param sequence: Sequence to remove adv. drawings
    """
    data = bpy.data
    sequence_editor = data.scenes["Scene"].sequence_editor

    # Scene
    drawing_scene = sequence.get("drawing_scene", None)

    if drawing_scene:
        data.scenes.remove(drawing_scene)
        sequence.pop("drawing_scene")

    # Rendered drawings and Externally edited images
    all_metas = get_parent_metas(sequence)
    if all_metas:
        parent_meta = all_metas[-1]

        # Open meta hierarchy
        # TODO gonna be removed with 2.92
        for meta in all_metas:
            meta.select = True
            sequence_editor.active_strip = meta

            bpy.ops.sequencer.meta_toggle()  # Open meta

        for seq in parent_meta.sequences:
            if seq.get("kind", None) in {"temp", "externally_edited"}:
                seq.select = True
            else:
                seq.select = False

        bpy.ops.sequencer.delete()

        # Clear rendered images reference
        sequence.pop("rendered_images", None)
        sequence.pop("external_images", None)

        # Close meta hierarchy
        # TODO gonna be removed with 2.92
        for meta in reversed(all_metas):
            # Deselect to avoid entering in a newly created meta
            bpy.ops.sequencer.select_all(action="DESELECT")

            sequence_editor.active_strip = None

            bpy.ops.sequencer.meta_toggle()  # Close meta


def clear_annotate(sequence: Sequence):
    """Clear annotate tool drawings.

    :param sequence: Sequence to remove annotate drawings
    """
    data = bpy.data

    for layer in data.grease_pencils["Annotations"].layers:
        for frame in layer.frames:
            if sequence.frame_start <= frame.frame_number < sequence.frame_final_end:
                layer.frames.remove(frame)


def clear_text_content(sequence: Sequence):
    """Clear text content from a sequence.

    :param sequence: Sequence to remove comments
    """
    # Delete text comments and replies
    for text in sequence.get("text_contents", []):
        bpy.data.texts.remove(text)

    # Remove the text contents from the sequence
    sequence.pop("text_contents", None)
