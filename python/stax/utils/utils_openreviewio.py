# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software Foundation,
#  Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# ##### END GPL LICENSE BLOCK #####
"""
Every utils function related to OpenReviewIO
"""

from pathlib import Path

import openreviewio as orio

from .utils_cache import get_orio_default_cache_directory


def get_cache_media_review(media_path: Path) -> orio.MediaReview:
    """Get media review in cache related to the provided media path

    :param media_path: Path to media
    :return: Media review
    """
    media_path = Path(media_path)

    review_path = get_orio_default_cache_directory().joinpath(f"{media_path.name}.orio")

    if review_path.is_dir():
        return orio.load_media_review(review_path)
    else:
        return None


def write_review_to_cache(review: orio.MediaReview):
    """Write media review to Stax cache

    :param review: Media Review to write
    """
    folder = get_orio_default_cache_directory()

    review.write(folder)


def available_statuses_callback(screen, context):
    """Get available statuses as items for enum property.

    :param screen: Blender current screen object
    :param context: Blender context
    """
    return [
        (status, status.capitalize(), "No description")
        for status in reversed(orio.allowed_statuses)
    ]
