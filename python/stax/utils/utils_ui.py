# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software Foundation,
#  Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# ##### END GPL LICENSE BLOCK #####
"""
Every utils function related to UI management
"""
import shutil
from pathlib import Path
import textwrap
from typing import List, Union
from datetime import datetime

import bpy
import bl_ui
import openreviewio as orio

import stax
from stax.properties.properties_core import Note, TextAnnotation
from .utils_core import get_text_body, get_text_contents, wrap_text
from .utils_timeline import get_media_sequence


def draw_set_preview_range(scene: bpy.types.Scene, layout: bpy.types.UILayout):
    """Draw Set Preview range operator

    By default shows a button to set the preview range with the mouse
    When preview is enabled, it allows to change the value by changing the frames start/end number
    Disabled if versions of a sequence are displayed
    """
    row = layout.row(align=True)
    if not scene.versions_displayed_sequence:
        if scene.use_preview_range:
            row.operator("anim.previewrange_clear", icon="CANCEL", text="")
            row.prop(scene, "frame_preview_start", text="")
            row.prop(scene, "frame_preview_end", text="")
        else:
            # If advanced drawing, only draw the Box Select way
            if bpy.context.workspace is bpy.data.workspaces["Advanced drawing"]:
                layout.operator("anim.previewrange_set", icon="PREVIEW_RANGE")
            else:
                layout.menu("DOPESHEET_MT_preview_range", icon="PREVIEW_RANGE")


def draw_emphasize_drawing_slider(layout):
    """Draw Emphasize drawings slider.

    :param layout: Layout to draw the slider in
    """
    row = layout.row(align=True)
    row.label(icon="LIGHT_SUN")
    row.prop(bpy.context.window_manager, "emphasize_drawings", text="")


def override_annotation_layers():
    """Override annotation layers panel functions to remove some UI

    Needed because AppOverrideState is not fully implemented.

    .. todo:: remove it when the core method is patched:

        template_ID removal and impossibility to disable:

            * ("GPencilLayer", "lock_frame")
            * "gpencil.annotation_active_frame_delete"
    """

    def draw_layers(self, context, layout, gpd):
        row = layout.row()

        col = row.column()
        if len(gpd.layers) >= 2:
            layer_rows = 5
        else:
            layer_rows = 3
        col.template_list(
            "GPENCIL_UL_annotation_layer",
            "",
            gpd,
            "layers",
            gpd.layers,
            "active_index",
            rows=layer_rows,
            sort_reverse=True,
            sort_lock=True,
        )

        col = row.column()

        sub = col.column(align=True)
        sub.operator("gpencil.stax_layer_annotation_add", icon="ADD", text="")
        sub.operator("gpencil.layer_annotation_remove", icon="REMOVE", text="")

        gpl = context.active_annotation_layer
        if gpl:
            if len(gpd.layers) > 1:
                col.separator()

                sub = col.column(align=True)
                sub.operator(
                    "gpencil.layer_annotation_move", icon="TRIA_UP", text=""
                ).type = "UP"
                sub.operator(
                    "gpencil.layer_annotation_move", icon="TRIA_DOWN", text=""
                ).type = "DOWN"

        tool_settings = context.tool_settings
        if gpd and gpl:
            layout.prop(gpl, "thickness")
        else:
            layout.prop(tool_settings, "annotation_thickness", text="Thickness")

    def annotation_layers_panel_draw(self, context):
        layout = self.layout
        layout.use_property_decorate = False

        # Grease Pencil owner
        gpd_owner = context.annotation_data_owner
        gpd = context.annotation_data

        # List of layers/notes
        if gpd and gpd.layers:
            draw_layers(self, context, layout, gpd)

    bl_ui.properties_grease_pencil_common.AnnotationDataPanel.draw = (
        annotation_layers_panel_draw
    )


def setup_custom_icons():
    """Copy custom Stax icons to Blender's "datafiles/icons"

    Might encounter an issue if the user has installed Blender in a system directory and (s)he is not admin.
    This is a known issue and this case must be handled by the user.
    """

    blender_icons_dir = Path(bpy.utils.system_resource("DATAFILES"), "icons")
    stax_icons_dir = (
        Path(stax.__file__).parent.parent.parent.joinpath("icons").resolve()
    )

    for icon in stax_icons_dir.iterdir():
        if icon.suffix == ".dat":
            shutil.copy(icon, blender_icons_dir.joinpath(icon.name))


def display_note(
    layout: bpy.types.UILayout, width: float, stax_note: Note
) -> bpy.types.UILayout:
    """Display note content in a box

    :param layout: Layout in which display the note
    :param width: Width of the panel for text wrapping
    :param stax_note: Stax Note to display contents of
    :return: Created box layout
    """
    if (
        stax_note.text_comments or stax_note.text_annotations
    ):  # Sentinel if no text to display
        wrapper = textwrap.TextWrapper(
            width=width, break_long_words=False, replace_whitespace=False
        )

        # Note header
        box = display_note_header(layout, stax_note.author, stax_note.date)

        # Body
        display_comments(
            box,
            wrapper,
            [comment.body for comment in stax_note.text_comments],
        )

        # Annotations
        for stax_text_annotation in stax_note.text_annotations:
            display_annotation(box, wrapper, stax_text_annotation)

        return box


def display_pending_note(
    layout: bpy.types.UILayout,
    width: float,
    author: str,
    sequence: str,
) -> bpy.types.UILayout:
    """Display pending note as greyed out normal text comment or annotation

    :param layout: Layout in which display the note
    :param width: Width of the panel for text wrapping
    :param author: Author of the comment
    :param sequence: Sequence linked to the note
    :return: Created box layout
    """
    wrapper = textwrap.TextWrapper(width=width)

    text_contents = sequence.get("text_contents", [])
    if text_contents:
        comments, annotations = get_text_contents(sequence.get("text_contents", []))
        # Display note header
        box = display_note_header(layout, author)

        # Display the pending comments
        # =========
        for comment_pad in comments:
            # Get text content from text pads
            body = get_text_body(comment_pad)

            if "".join(body):  # Sentinel if empty text
                row = box.row()

                # Display body
                display_comments(row, wrapper, [body])

                # Buttons
                display_edition_buttons(row, comment_pad.name, move_playhead=False)

        # Display the pending annotations
        # =========
        for annotation_pad in annotations:
            annotation_body = get_text_body(annotation_pad)

            row = box.row()

            # Display annotations
            orio_text_annotation = orio.Content.TextAnnotation(
                body=annotation_body,
                frame=annotation_pad.get("frame_start"),
                duration=annotation_pad.get("frame_end")
                - annotation_pad.get("frame_start"),
            )
            display_annotation(
                row,
                wrapper,
                orio_text_annotation,
            )

            # Buttons
            display_edition_buttons(row, annotation_pad.name)

        box.active = False  # Grey it out

        return box


def display_pending_reply(
    layout: bpy.types.UILayout,
    width: float,
    author: str,
    date: datetime,
    textpad_name: str,
) -> bpy.types.UILayout:
    """Display pending note as greyed out normal text comment or annotation

    :param layout: Layout in which display the note
    :param width: Width of the panel for text wrapping
    :param author: Author of the comment
    :param date: Date of the comment
    :param textpad_name: Name of the text pad that contains the pending comment
    :return: Created box layout
    """

    # Get text content from text pads
    text = bpy.data.texts.get(textpad_name)
    if text:
        body = get_text_body(text)

        if not "".join(body):  # Sentinel if empty text
            return

        # Display the pending reply
        # =========
        row = layout.row()

        # Display note header
        box = display_note_header(row, author)

        # Display the content
        wrapper = textwrap.TextWrapper(width=width)
        display_comments(
            box,
            wrapper,
            [body],
        )

        # Buttons
        display_edition_buttons(row, textpad_name, move_playhead=False)

        box.active = False  # Grey it out

        return layout


def display_comments(
    layout: bpy.types.UILayout,
    wrapper: textwrap.TextWrapper,
    text_comments: List[str],
) -> bpy.types.UILayout:
    """Display text comments body

    :param layout: Layout in which display the note
    :param wrapper: Wrapper to wrap the text
    :param text_comments: Comments to display
    :return: Created box layout
    """
    col = layout.column(align=True)
    for comment in text_comments:
        wrapped_lines = wrap_text(wrapper, comment)
        for line in wrapped_lines:
            sub = col.row()
            sub.alignment = "EXPAND"
            sub.label(text=line)

        col.separator()

    return col


def display_note_header(layout: bpy.types.UILayout, author: str, date=""):
    """Display note header with user name and date

    :param layout: Layout do display into
    :param author: Author of the note
    :param date: Date of note's creation, defaults to
    """
    # Create box
    box = layout.box()

    # Header
    col = box.column(align=True)
    col.label(text=author, icon="USER")
    if date:
        col.label(
            text=datetime.fromisoformat(date).strftime("%m/%d/%Y, %H:%M:%S"),
            icon="TIME",
        )

    box.separator()

    return box


def display_annotation(
    layout: bpy.types.UILayout,
    wrapper: textwrap.TextWrapper,
    text_annotation: List[Union[orio.Content.TextAnnotation, TextAnnotation]],
) -> bpy.types.UILayout:
    """Display text annotations

    :param layout: Layout in which display the note
    :param wrapper: Wrapper to wrap the text
    :param text_annotation: Text annotations to display
    :return: Created box layout
    """
    scene = bpy.context.scene
    current_sequence = bpy.data.scenes["Scene"].sequence_editor.active_strip
    media_sequence = get_media_sequence(current_sequence)

    # Display text annotation
    # =======================
    sub_col = layout.column(align=True)

    # Text Annotation Header
    # -------
    row = sub_col.row(align=True)

    # Absolute values, start and end frame are limited to current sequence boundaries if beyond
    raw_annotation_start_frame = (
        media_sequence.frame_start
        - media_sequence.animation_offset_start
        + text_annotation.frame
    )
    absolute_start_frame = max(
        current_sequence.frame_start,
        raw_annotation_start_frame,
    )
    absolute_end_frame = min(
        raw_annotation_start_frame + text_annotation.duration,
        current_sequence.frame_final_end - 1,
    )

    # Button to handle preview range: if the current preview is the same range of the annotation, display a cross to remove it
    if scene.use_preview_range and (
        scene.frame_preview_start == absolute_start_frame
        and scene.frame_preview_end == absolute_end_frame
    ):
        row.prop(scene, "use_preview_range", icon="PREVIEW_RANGE", text="")
    else:
        # Button to automatically set the preview range
        ops = row.operator(
            "sequencer.show_text_annotation",
            text="",
            icon="PREVIEW_RANGE",
        )

        # if absolute_end_frame - absolute_start_frame > 1:
        ops.frame_start = absolute_start_frame
        ops.frame_end = absolute_end_frame
        ops.set_preview_range = True

    # Label
    # If duration is greater than 0, display the range, else only the frame
    if text_annotation.duration > 1:
        row.label(
            text=f"[{text_annotation.frame} --> {text_annotation.frame + text_annotation.duration}]"
        )
    else:
        row.label(text=f"[{text_annotation.frame}]")
    # -------

    sub_col.separator()

    # Wrap text and display
    wrapped_lines = wrap_text(wrapper, text_annotation.body)

    for line in wrapped_lines:
        row = sub_col.row()
        row.alignment = "LEFT"

        # Display line as operator to be able to click on it
        ops = row.operator(
            "sequencer.show_text_annotation",
            text=line,
            emboss=False,
            depress=True,
        )
        # Set preview range only if wider than 1
        # if absolute_end_frame - absolute_start_frame > 1:
        ops.frame_start = absolute_start_frame
        ops.frame_end = absolute_end_frame


def display_edition_buttons(
    layout: bpy.types.UILayout, textpad_name: str, move_playhead=True
):
    """Display edition buttons in a column: Delete, Edit.

    :param layout: Layout to insert the column into
    :param textpad_name: Text to associate to the buttons
    :param move_playhead: Move playhead to first frame of annotation, defaults to True
    """
    col = layout.column()

    # Delete button
    col.operator("wm.delete_comment", icon="TRASH", text="").textpad_name = textpad_name

    # Edit button, do not move playhead to first frame of comment
    ops = col.operator("wm.edit_comment", icon="LINE_DATA", text="")
    ops.text_name = textpad_name
    ops.move_playhead = False


def open_text_window(text_pad):
    context = bpy.context

    # List preview areas
    preview_areas = [
        area for area in bpy.data.screens["Console"].areas if area.type == "CONSOLE"
    ]

    # Get the existing window if there is one and delete it
    for win in context.window_manager.windows:
        if win.screen.name.startswith("Writing"):
            writing_window = win
            bpy.ops.wm.window_close({"window": win})
            break

    # Duplicate preview area
    if preview_areas:
        bpy.ops.screen.area_dupli(
            {
                "screen": bpy.data.screens["Console"],
                "area": preview_areas[-1],
                "window": context.window_manager.windows[0],
            },
            "INVOKE_DEFAULT",
        )

    # Get new window
    writing_window = context.window_manager.windows[-1]
    writing_window.screen.name = "Writing"

    # Get wanted area
    area = [x for x in writing_window.screen.areas if x.type == "CONSOLE"][0]

    # Change area type
    area.type = "TEXT_EDITOR"
    area.spaces[0].show_syntax_highlight = False

    # Resize window
    # There's no way to resize window, screen or area in Blender 2.82a API...

    # Load text in the editor
    area.spaces[0].text = text_pad

    # Set cursor to the first line to avoid display issue
    text_pad.cursor_set(0)


def get_stax_icon(icon_name: str) -> bpy.types.ImagePreview:
    """Get Stax custom icon by its name.

    :param icon_name: Name of the wanted icon
    :return: Icon Image Preview
    """
    icons_previews = stax.preview_collections["icons"]
    return icons_previews[icon_name]
