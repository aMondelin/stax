# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software Foundation,
#  Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# ##### END GPL LICENSE BLOCK #####
"""
Every utils function related to the linker
"""

import os
import sys
from pathlib import Path
import inspect
from typing import Union, List
from types import ModuleType
import importlib.util as import_util

import bpy

from stax.Callbacks_Script import Callbacks
from . import utils_config


def get_callbacks_scripts() -> List[Path]:
    """Get all available callbacks scripts.

    :return: List of callbacks scripts
    """
    if os.getenv("STAX_LINKER_PATH"):
        callbacks_dir_path = Path(os.getenv("STAX_LINKER_PATH"))

        if not callbacks_dir_path.is_file() or callbacks_dir_path.suffix != ".py":
            raise RuntimeError("STAX_LINKER_PATH is not pointing to a python file.")

        callbacks_scripts_paths = [callbacks_dir_path]

    elif os.getenv("STAX_LINKER_FOLDER"):
        callbacks_dir_path = Path(os.getenv("STAX_LINKER_FOLDER"))

        if not callbacks_dir_path.is_dir():
            raise RuntimeError("STAX_LINKER_FOLDER is not pointing to a folder.")

        callbacks_scripts_paths = [
            p
            for p in callbacks_dir_path.iterdir()
            if p.suffix == ".py" and p.stem != "__init__"
        ]

        if not callbacks_scripts_paths:
            raise RuntimeError(
                "No callback script found, add some in the STAX_LINKER_FOLDER path."
            )

    else:  # TODO: Create operator to import linker by hand and copy it in this folder

        callbacks_dir_path = Path(utils_config.get_config_directory(), "callbacks")

        if not callbacks_dir_path.is_dir():
            callbacks_dir_path.mkdir(parents=True)

        callbacks_scripts_paths = [
            p
            for p in callbacks_dir_path.iterdir()
            if p.suffix == ".py" and p.stem != "__init__"
        ]

    return callbacks_scripts_paths


def load_script_as_module(script_path: Union[Path, str]) -> ModuleType:
    """Load given script path as module.

    :param script_path: Script path to load
    :return: Module
    """
    module_key = str(script_path.stem)
    if module_key in sys.modules:
        return sys.modules[module_key]

    spec = import_util.spec_from_file_location(script_path.stem, script_path)
    module = import_util.module_from_spec(spec)
    sys.modules[module_key] = module
    spec.loader.exec_module(module)

    return module


def get_available_callbacks() -> List[Callbacks]:
    """Get all available callbacks classes.

    :return: Dict of callbacks classes. The key is the raw name of the callback script without suffix.
    """
    callbacks_scripts = get_callbacks_scripts()

    available_callbacks = {}

    for callback in callbacks_scripts:
        # Load script as modules to access members
        module = load_script_as_module(callback)

        for x in dir(module):
            m = getattr(module, x)

            # Test if member is as class and is direct from Callbacks class
            if inspect.isclass(m) and Callbacks in m.__mro__:
                # callback name without suffix used as key
                available_callbacks[callback.stem] = m

    return available_callbacks


def get_linkers_list_enum():
    """Return the list of the callbacks formatted for Blender enum.

    :return: Tuples list
    """
    available_callbacks = get_available_callbacks()
    return [
        (name, name.replace("_", " "), available_callbacks[name].description)
        for name in available_callbacks.keys()
    ]


def get_linker():
    """Get the linker as ProductionManager() object.

    :return: Linker as ProductionManager() object
    """
    if hasattr(bpy.context.window_manager, "stax_callback"):
        return bpy.context.window_manager.stax_callback


def check_linker_func(attribute: str) -> bool:
    """Check if the linker has disabled the given function (callable)

    :param attribute: Attribute to be tested
    :return: Boolean for attribute's presence
    """
    linker = get_linker()
    if linker and hasattr(linker, attribute):
        attribute = getattr(linker, attribute)
        return callable(attribute)
    return False
