# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software Foundation,
#  Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# ##### END GPL LICENSE BLOCK #####
"""
Every ui class (panel, menu or header) relative to the export tool
"""

import bpy


class RENDER_PT_ExportSelection(bpy.types.Panel):
    """Panel for selecting elements to export"""

    bl_idname = "RENDER_PT_export_selection"
    bl_label = "Export Selection"
    bl_space_type = "PROPERTIES"
    bl_region_type = "WINDOW"
    bl_context = "view_layer"

    @classmethod
    def poll(cls, context):
        return context.workspace is bpy.data.workspaces["Timeline export"]

    def draw(self, context):
        layout = self.layout
        scene = context.scene

        # Button: New render track
        if len(scene.render_tracks) == 0:
            layout.alert = True
        if len(scene.render_tracks) < 4:
            layout.operator("scene.new_render_track")

        layout.separator()
        row = layout.row()
        for render_track in scene.render_tracks:
            col = row.column(align=True)
            sub = col.row(align=True)

            # Select render track or remove it
            sub.prop_search(render_track, "name", scene, "tracks", text="")
            sub.operator(
                "scene.remove_render_track", icon="REMOVE", text=""
            ).name = render_track.name

            # Custom Title
            sub = col.row(align=True)
            sub.alignment = "RIGHT"
            sub.label(text="Title: ")
            sub.label(text="".join([render_track.name, " -- "]))
            sub.prop(render_track, "comment", text="")

            # Draw render track elements based on tracks
            col.separator()
            if render_track.name:
                for element in scene.tracks.get(render_track.name).elements:
                    col.prop(element, "render", text=element.name)


class RENDER_PT_ExportRender(bpy.types.Panel):
    """Panel for rendering the timeline"""

    bl_idname = "RENDER_PT_export_render"
    bl_label = "Export Render"
    bl_space_type = "PROPERTIES"
    bl_region_type = "WINDOW"
    bl_context = "output"

    @classmethod
    def poll(cls, context):
        return context.workspace is bpy.data.workspaces["Timeline export"]

    def draw(self, context):
        layout = self.layout
        scene = context.scene

        # Button: Render
        if len(scene.render_tracks) > 0:
            layout.operator("scene.export_render")


classes = [RENDER_PT_ExportSelection, RENDER_PT_ExportRender]


def register():
    for cls in classes:
        bpy.utils.register_class(cls)


def unregister():
    for cls in reversed(classes):
        bpy.utils.unregister_class(cls)
