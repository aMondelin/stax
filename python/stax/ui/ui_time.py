# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software Foundation,
#  Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# ##### END GPL LICENSE BLOCK #####

import bpy
from bpy.types import Header

from stax.utils import utils_linker
from stax.utils.utils_timeline import get_media_sequence


class TIME_HT_editor_buttons(Header):
    bl_space_type = "DOPESHEET_EDITOR"
    bl_label = ""

    def draw(self, context):
        pass

    @staticmethod
    def draw_header(context, layout):
        scene = context.scene
        screen = context.screen
        user_preferences = scene.user_preferences

        # User settings
        row = layout.row(align=True)
        row.operator("scene.change_stax_configuration", text="", icon="PREFERENCES")

        # Log-in Button
        # If callbacks script implemented authentication_exec then propose logging button
        # Else get system user name
        if utils_linker.check_linker_func("authentication_exec"):
            if not scene.session_logged_in:
                linker = utils_linker.get_linker()
                row.operator(
                    "wm.pm_authentication",
                    text="Please log-in to " + linker.name,
                    icon="USER",
                )
            else:
                row.operator(
                    "wm.pm_authentication",
                    text="user : " + user_preferences.user_login,
                    icon="USER",
                )

                # UI displayed if user logging is successful
                if scene is bpy.data.scenes["Scene"]:
                    # [] Load timeline
                    row.separator()
                    row.operator("sequencer.load_timeline", icon="ASSET_MANAGER")

                    # [] Link reviews, displayed if sequences
                    if scene.timeline or scene.sequence_editor.sequences:
                        row.operator("scene.link_reviews", icon="DOCUMENTS")

        else:
            row.label(text=user_preferences.user_login)

            layout.separator()
            if scene is bpy.data.scenes["Scene"]:
                row = layout.row()

                # [] Load timeline
                row.operator("sequencer.load_timeline", icon="ASSET_MANAGER")

                # [] Link reviews, displayed if sequences
                if scene.timeline or scene.sequence_editor.sequences:
                    row.operator("scene.link_reviews", icon="DOCUMENTS")

                # Disable Link/Load buttons when review session is active to avoid session reset
                row.enabled = not context.scene.review_session_active

        # Update timeline
        # If timeline cached and scene is Main, display appropriate UI
        if scene.is_timeline_loaded and scene is bpy.data.scenes["Scene"]:
            # Update current timeline
            row = layout.row()
            row.operator("sequencer.update_session", text="Update", icon="FILE_REFRESH")

            # Disable Update button when review session is active to avoid session reset
            row.enabled = not context.scene.review_session_active

        layout.separator_spacer()

        # Playback buttons
        row = layout.row(align=True)
        row.operator("screen.frame_jump", text="", icon="REW").end = False
        row.operator("screen.keyframe_jump", text="", icon="PREV_KEYFRAME").next = False
        if not screen.is_animation_playing:
            # if using JACK and A/V sync:
            #   hide the play-reversed button
            #   since JACK transport doesn't support reversed playback
            if (
                scene.sync_mode == "AUDIO_SYNC"
                and context.preferences.system.audio_device == "JACK"
            ):
                row.scale_x = 2
                row.operator("screen.animation_play", text="", icon="PLAY")
                row.scale_x = 1
            else:
                row.operator(
                    "screen.animation_play", text="", icon="PLAY_REVERSE"
                ).reverse = True
                row.operator("screen.animation_play", text="", icon="PLAY")
        else:
            row.scale_x = 2
            row.operator("screen.animation_play", text="", icon="PAUSE")
            row.scale_x = 1
        row.operator("screen.keyframe_jump", text="", icon="NEXT_KEYFRAME").next = True
        row.operator("screen.frame_jump", text="", icon="FF").end = True

        layout.separator_spacer()

        # Current frame
        row = layout.row(align=True)
        row.scale_x = 0.9
        row.prop(scene, "frame_current", text="")
        if scene.sequence_editor.active_strip:
            row.prop(scene, "media_current_frame", text="Media")

        # Start and End frame
        row = layout.row(align=True)
        row.scale_x = 0.8
        row.prop(scene, "frame_start", text="Start")
        row.prop(scene, "frame_end", text="End")


classes = [TIME_HT_editor_buttons]


def register():
    for cls in classes:
        bpy.utils.register_class(cls)


def unregister():
    for cls in reversed(classes):
        bpy.utils.unregister_class(cls)
