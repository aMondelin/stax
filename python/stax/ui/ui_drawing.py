# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software Foundation,
#  Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# ##### END GPL LICENSE BLOCK #####

# <pep8 compliant>

"""
Every ui class (panel, menu or header) relative to drawing
"""

import bpy
import bl_ui
from bl_ui.space_view3d import _draw_tool_settings_context_mode
from bpy.types import Header, Panel, UIList

from . import ui_time
from .ui_notes import NOTES_PT_pending_text_note, NOTES_PT_text_notes
from stax.utils.utils_ui import (
    draw_set_preview_range,
    draw_emphasize_drawing_slider,
    get_stax_icon,
)


class TOPBAR_PT_gpencil_materials(Panel):
    """Panel to choose pencil materials

    Override of Blender class.
    """

    bl_label = "Grease Pencil Material Slots"
    bl_space_type = "PROPERTIES"
    bl_region_type = "WINDOW"
    bl_context = "material"
    bl_options = {"HIDE_HEADER"}

    def draw(self, context):
        layout = self.layout
        ob = context.object

        # Draw UIList on clicking
        row = layout.row()
        if ob:
            row.template_list(
                "GPENCIL_UL_matslots",
                "",
                ob,
                "material_slots",
                ob,
                "active_material_index",
                rows=2,
            )


class GPENCIL_UL_matslots(UIList):
    """List for materials

    Override of Blender class.
    """

    def draw_item(
        self,
        _context,
        layout,
        _data,
        item,
        icon,
        _active_data,
        _active_propname,
        _index,
    ):
        mat = item.material
        if (mat is not None) and (mat.grease_pencil is not None):
            gpcolor = mat.grease_pencil

            # Grey out the layout
            if gpcolor.lock:
                layout.active = False

            # Material name
            row = layout.row(align=True)
            row.enabled = not gpcolor.lock
            row.prop(mat, "name", text="", emboss=False, icon_value=icon)

            # Enable it for Onion skins
            row = layout.row(align=True)
            if gpcolor.ghost is True:
                icon = "ONIONSKIN_OFF"
            else:
                icon = "ONIONSKIN_ON"
            row.prop(gpcolor, "ghost", text="", icon=icon, emboss=False)
            row.prop(gpcolor, "hide", text="", emboss=False)  # Hide it


class GPENCIL_UL_layer(UIList):
    """List of layers

    Override of Blender class.
    """

    # TODO add UIList to AppOverrideState.ui_ignore_classes, submit patch to Blender
    # Will avoid to have to copy this whole class to remove only one parameter: "use_mask_layer"
    def draw_item(
        self,
        _context,
        layout,
        _data,
        item,
        icon,
        _active_data,
        _active_propname,
        _index,
    ):
        gpl = item

        # Grey out the layout
        if gpl.lock:
            layout.active = False

        # Layer Name
        row = layout.row()
        row.prop(gpl, "info", text="")

        # Layer settings
        row = layout.row(align=True)
        subrow = row.row(align=True)
        subrow.prop(
            gpl,
            "use_onion_skinning",
            text="",
            icon="ONIONSKIN_ON" if gpl.use_onion_skinning else "ONIONSKIN_OFF",
            emboss=False,
        )
        row.prop(gpl, "hide", text="", emboss=False)
        row.prop(gpl, "lock", text="", emboss=False)


class TOPBAR_PT_gpencil_vertexcolor(Panel):
    """Popover drawing color

    Override of Blender class.
    """

    bl_space_type = "VIEW_3D"
    bl_region_type = "HEADER"
    bl_label = "Vertex Color"
    bl_ui_units_x = 10

    def draw(self, context):
        layout = self.layout

        ts = context.scene.tool_settings
        is_vertex = context.mode == "VERTEX_GPENCIL"
        gpencil_paint = ts.gpencil_vertex_paint if is_vertex else ts.gpencil_paint
        brush = gpencil_paint.brush

        if context.object:
            col = layout.column()

            # Color picker
            col.template_color_picker(brush, "color", value_slider=True)

            # Color switch
            sub_row = layout.row(align=True)
            sub_row.prop(brush, "color", text="")
            sub_row.prop(brush, "secondary_color", text="")

            sub_row.operator("gpencil.tint_flip", icon="FILE_REFRESH", text="")

            # Palette
            if gpencil_paint.palette:
                layout.template_palette(gpencil_paint, "palette", color=True)


class VIEW3D_PT_gpencil_onion_skinning(Panel):
    """Custom panel for Onion Skinning settings"""

    bl_space_type = "VIEW_3D"
    bl_region_type = "HEADER"
    bl_label = "Onion Skinning"

    def draw(self, context):
        layout = self.layout

        gpd = context.active_object.data

        layout.use_property_split = True

        # Colors
        col = layout.column()
        col.prop(gpd, "before_color", text="Before")
        col.prop(gpd, "after_color", text="After")

        # Globals
        col.prop(gpd, "onion_factor", text="Opacity", slider=True)

        # Range before and after
        col = layout.column(align=True)
        col.prop(gpd, "onion_mode")
        if gpd.onion_mode == "ABSOLUTE":
            col.prop(gpd, "ghost_before_range", text="Frames Before")
            col.prop(gpd, "ghost_after_range", text="Frames After")
        elif gpd.onion_mode == "RELATIVE":
            col.prop(gpd, "ghost_before_range", text="Keyframes Before")
            col.prop(gpd, "ghost_after_range", text="Keyframes After")


class VIEW3D_HT_header(Header):
    """Topbar view tools

    Override of Blender class.
    """

    bl_space_type = "VIEW_3D"

    def draw(self, context):
        layout = self.layout
        tool_mode = context.mode

        view = context.space_data
        main_scene = bpy.data.scenes["Scene"]

        # Stax tool
        if context.scene is not main_scene:
            # Select edit/draw mode
            row = layout.row(align=True)
            modes = [
                ("PAINT_GPENCIL", "Draw", "GREASEPENCIL"),
                ("EDIT_GPENCIL", "Edit", "EDITMODE_HLT"),
            ]
            for mode, name, icon in modes:
                row.operator(
                    "object.mode_set", depress=tool_mode == mode, text=name, icon=icon
                ).mode = mode

            # Display Cancel/Complete
            self.draw_stax_tools()

        # Put it on right side
        layout.separator_spacer()

        # Show/Hide GPencil
        layout.prop(
            context.space_data,
            "show_object_viewport_grease_pencil",
            text="",
            icon="STROKE",
        )

        # Emphasize drawings value
        draw_emphasize_drawing_slider(layout)

        # Onion skins toggle
        onion_skin_icon = (
            "ONIONSKIN_ON" if view.overlay.use_gpencil_onion_skin else "ONIONSKIN_OFF"
        )
        row = layout.row(align=True)
        row.prop(view.overlay, "use_gpencil_onion_skin", icon=onion_skin_icon, text="")
        # Panel popover
        row.popover(panel="VIEW3D_PT_gpencil_onion_skinning", text="")

        # Flip Horizontally & Vertically
        background_media = context.scene.camera.data.background_images[0]
        row = layout.row(align=True)
        row.scale_x = 0.7
        row.prop(background_media, "use_flip_x", text="X", toggle=True)
        row.prop(background_media, "use_flip_y", text="Y", toggle=True)

    def draw_stax_tools(self):
        """Draw Stax tools operators"""
        layout = self.layout

        # Drawing cancel/complete
        # -----------------------
        row = layout.row(align=True)

        # Cancel
        row.operator(
            "wm.advanced_drawing",
            text="Cancel",
            icon="CANCEL",
        ).cancel = True
        # Validate
        row.operator(
            "wm.advanced_drawing",
            text="Complete drawing",
            icon="OUTLINER_DATA_GP_LAYER",
        ).start = False


class VIEW3D_HT_tool_header(Header):
    """Toolbar header

    Override of Blender class.
    """

    bl_space_type = "VIEW_3D"
    bl_region_type = "TOOL_HEADER"

    def draw(self, context):
        layout = self.layout

        main_scene = bpy.data.scenes["Scene"]

        # Sentinel if no media
        if not hasattr(main_scene.sequence_editor, "active_strip"):
            layout.separator_spacer()
            layout.label(text="No media to review.")
            layout.separator_spacer()
            return

        # If the current scene is the main one then setup, else display tools
        if context.scene is main_scene:
            layout.separator_spacer()
            if main_scene.sequence_editor.active_strip.lock:
                layout.alert = True
                layout.label(
                    text="You're not allowed to review this media", icon="ERROR"
                )
                layout.alert = False
            else:
                # Notify the user this is not the best way to access the Adv. Drawing mode
                layout.label(
                    text="Use the Advanced Drawing button in the Main view to access the Advanced Drawing mode or click on ->",
                    icon="INFO",
                )

                # Setup Advanced drawing
                layout.operator(
                    "wm.advanced_drawing",
                    icon="STROKE",
                    text="Setup Advanced Drawing",
                )

            layout.separator_spacer()
        else:
            layout = self.layout
            tool_mode = context.mode

            # Active Tool Settings
            # ----------------------
            from bl_ui.space_toolsystem_common import ToolSelectPanelHelper

            tool = ToolSelectPanelHelper.draw_active_tool_header(
                context,
                layout,
                tool_key=("VIEW_3D", tool_mode),
            )

            # Draw settings
            draw_fn = getattr(_draw_tool_settings_context_mode, tool_mode, None)
            if draw_fn is not None:
                is_valid_context = draw_fn(context, layout, tool)

            layout.separator_spacer()

            # Editing tools
            bl_ui.space_view3d.VIEW3D_HT_header.draw_xform_template(layout, context)

            layout.separator()

            # Fill opacity
            layout.prop(context.scene, "fill_opacity", slider=True)

            # Add empty keyframe for current layer
            layout.operator("scene.add_empty_gp_frame", icon="KEYFRAME_HLT")

            layout.separator_spacer()

            # Tool mode settings
            bl_ui.space_view3d.VIEW3D_HT_tool_header.draw_mode_settings(self, context)

            # Add layer button
            layout.operator("gpencil.stax_layer_add", text="", icon="ADD")

            layout.separator_spacer()

            # Toggle text notes
            layout.prop(
                context.space_data,
                "show_region_ui",
                icon="INFO",
                text="",
            )


class TOPBAR_PT_gpencil_layers(Panel):
    """Override of the native Blender Panel"""

    bl_space_type = "VIEW_3D"
    bl_region_type = "HEADER"
    bl_label = "Layers"
    bl_ui_units_x = 14

    @classmethod
    def poll(cls, context):
        if context.gpencil_data is None:
            return False

        ob = context.object
        if ob is not None and ob.type == "GPENCIL":
            return True

        return False

    def draw(self, context):
        layout = self.layout
        gpd = context.gpencil_data

        # Grease Pencil data...
        if (gpd is None) or (not gpd.layers):
            layout.operator("gpencil.stax_layer_add", text="New Layer")
        else:
            self.draw_layers(context, layout, gpd)

    def draw_layers(self, context, layout, gpd):
        row = layout.row()

        col = row.column()
        layer_rows = 10
        col.template_list(
            "GPENCIL_UL_layer",
            "",
            gpd,
            "layers",
            gpd.layers,
            "active_index",
            rows=layer_rows,
            sort_reverse=True,
            sort_lock=True,
        )

        gpl = context.active_gpencil_layer
        if gpl:
            srow = col.row(align=True)
            srow.prop(gpl, "blend_mode", text="Blend")

            srow = col.row(align=True)
            srow.prop(gpl, "opacity", text="Opacity", slider=True)
            srow.prop(
                gpl,
                "use_mask_layer",
                text="",
                icon="MOD_MASK" if gpl.use_mask_layer else "LAYER_ACTIVE",
            )

            srow = col.row(align=True)
            srow.prop(gpl, "use_lights")

        col = row.column()

        sub = col.column(align=True)
        sub.operator("gpencil.stax_layer_add", icon="ADD", text="")
        sub.operator("gpencil.layer_remove", icon="REMOVE", text="")

        gpl = context.active_gpencil_layer
        if gpl:
            sub.menu("GPENCIL_MT_layer_context_menu", icon="DOWNARROW_HLT", text="")

            if len(gpd.layers) > 1:
                col.separator()

                sub = col.column(align=True)
                sub.operator("gpencil.layer_move", icon="TRIA_UP", text="").type = "UP"
                sub.operator(
                    "gpencil.layer_move", icon="TRIA_DOWN", text=""
                ).type = "DOWN"

                col.separator()

                sub = col.column(align=True)
                sub.operator(
                    "gpencil.layer_isolate", icon="HIDE_OFF", text=""
                ).affect_visibility = True
                sub.operator(
                    "gpencil.layer_isolate", icon="LOCKED", text=""
                ).affect_visibility = False


class DOPESHEET_HT_header(Header):
    """Dopesheet header

    Override of Blender class.
    """

    bl_space_type = "DOPESHEET_EDITOR"

    def draw(self, context):
        layout = self.layout

        space_data = context.space_data

        if space_data.mode == "TIMELINE":
            ui_time.TIME_HT_editor_buttons.draw_header(context, layout)
        else:
            bl_ui.space_dopesheet.DOPESHEET_MT_editor_menus.draw_collapsible(
                context, layout
            )

            layout.separator_spacer()

            draw_set_preview_range(context.scene, layout)

            layout.separator_spacer()


class DOPESHEET_MT_editor_menus(bpy.types.Menu):
    bl_idname = "DOPESHEET_MT_editor_menus"
    bl_label = ""

    def draw(self, context):
        layout = self.layout
        st = context.space_data

        layout.menu("DOPESHEET_MT_select")

        if st.mode == "DOPESHEET" or (st.mode == "ACTION" and st.action is not None):
            layout.menu("DOPESHEET_MT_channel")
        elif st.mode == "GPENCIL":
            layout.menu("DOPESHEET_MT_gpencil_channel")

        if st.mode != "GPENCIL":
            layout.menu("DOPESHEET_MT_key")
        else:
            layout.menu("DOPESHEET_MT_gpencil_frame")


class VIEW3D_PT_stax_tools(bpy.types.Panel):
    """Add a custom tool panel in the Preview toolbar"""

    bl_label = " "
    bl_space_type = "VIEW_3D"
    bl_region_type = "TOOLS"
    bl_parent_id = "VIEW3D_PT_tools_active"
    bl_options = {"HIDE_HEADER"}

    def draw(self, context):
        layout = self.layout
        layout.scale_y = 2

        active_strip = bpy.data.scenes["Scene"].sequence_editor.active_strip
        # If review is enabled
        if active_strip and not active_strip.lock:
            # Write text comment
            write_comment_icon = get_stax_icon("write_comment")
            layout.operator(
                "wm.write_comment", icon_value=write_comment_icon.icon_id, text=""
            )


class VIEW3D_PT_pending_text_note(NOTES_PT_pending_text_note):
    """Pending Text note into Advanced Drawing"""

    bl_space_type = "VIEW_3D"


class VIEW3D_PT_text_notes(NOTES_PT_text_notes):
    """Text notes into Advanced Drawing"""

    bl_space_type = "VIEW_3D"


classes = [
    TOPBAR_PT_gpencil_materials,
    TOPBAR_PT_gpencil_vertexcolor,
    TOPBAR_PT_gpencil_layers,
    VIEW3D_PT_gpencil_onion_skinning,
    GPENCIL_UL_matslots,
    GPENCIL_UL_layer,
    VIEW3D_HT_tool_header,
    VIEW3D_HT_header,
    DOPESHEET_HT_header,
    DOPESHEET_MT_editor_menus,
    VIEW3D_PT_stax_tools,
    VIEW3D_PT_pending_text_note,
    VIEW3D_PT_text_notes,
]


def update_fill_opacity(self, _context):
    """Update fill opacity value for all fills"""
    bpy.data.materials["Fill"].grease_pencil.fill_color[3] = self.fill_opacity


def register():
    for cls in classes:
        bpy.utils.register_class(cls)

    # Override for fill opacity
    bpy.types.Scene.fill_opacity = bpy.props.FloatProperty(
        name="Fill opacity", update=update_fill_opacity, min=0, max=1, default=1
    )


def unregister():
    del bpy.types.Scene.fill_opacity

    for cls in reversed(classes):
        bpy.utils.unregister_class(cls)
