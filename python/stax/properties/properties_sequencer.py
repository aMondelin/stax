# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software Foundation,
#  Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# ##### END GPL LICENSE BLOCK #####
"""
Every class relative to sequencer behavior
"""

import bpy
from bpy.types import Sequence, WipeSequence

from stax.utils.utils_timeline import (
    add_single_driver,
    emphasize_drawings,
    compare_refresh,
    get_media_sequence,
    get_wipe_sequence,
    set_reviewed_state,
)
from stax.utils.utils_openreviewio import available_statuses_callback


def compare_setup(compare_properties, context):
    """Change blend type of the displayed strip.

    :param compare_properties: Blender sequencer properties object
    :param context: Blender context
    """
    scene = context.scene
    sequence_editor = context.scene.sequence_editor
    current_sequence = sequence_editor.active_strip

    # Reset wipe values to avoid persistent wipe
    if compare_properties.compare_type == "WIPE":
        compare_properties.wipe_fader = 0.5
    else:
        compare_properties.wipe_fader = 0

    def reset_wipe(wipe_sequence: WipeSequence):
        """Reset wipe value and remove driver"""
        wipe_sequence.driver_remove("effect_fader")
        wipe_sequence.effect_fader = 1

    def reset_alpha(sequence: Sequence):
        """Reset alpha value and remove driver"""
        sequence.driver_remove("blend_alpha")
        sequence.effect_fader = 1

    # Update all sequences on the same channel of the current sequence depending on the compare type
    for seq in sequence_editor.sequences:
        media_sequence = get_media_sequence(seq)
        if seq.channel == current_sequence.channel:
            # Get wipe sequence
            wipe_sequence = get_wipe_sequence(media_sequence)

            # Alpha setting
            if compare_properties.compare_type == "CROSS":
                # Add driver
                add_single_driver(
                    seq,
                    "blend_alpha",
                    "SCENE",
                    scene,
                    "compare_properties.opacity",
                )

                # Reset wipe
                reset_wipe(wipe_sequence)

            # Wipe
            elif compare_properties.compare_type == "WIPE":
                # Add driver
                add_single_driver(
                    wipe_sequence,
                    "effect_fader",
                    "SCENE",
                    scene,
                    "compare_properties.wipe_fader",
                )

                # Change sequence blend type
                seq.blend_type = "ALPHA_OVER"
            else:
                # Change sequence blend type
                seq.blend_type = compare_properties.compare_type

                # Reset alpha value
                reset_alpha(seq)

                # Reset wipe
                reset_wipe(wipe_sequence)
        else:
            seq.blend_type = "CROSS"

            # Reset alpha value
            reset_alpha(seq)

            # Get wipe sequence
            wipe_sequence = get_wipe_sequence(media_sequence)

            # Reset wipe
            reset_wipe(wipe_sequence)

    # Refresh the sequencer TODO: probably won't be needed after this fix: https://developer.blender.org/T79922
    bpy.ops.sequencer.refresh_all()

    # Set current sequence active back
    if current_sequence:
        bpy.ops.sequencer.select_all(action="DESELECT")
        sequence_editor.active_strip = current_sequence
        current_sequence.select = True


def change_media_status(self, context):
    """Change status of the current displayed media"""
    media_sequence = get_media_sequence(
        bpy.data.scenes["Scene"].sequence_editor.active_strip
    )
    if media_sequence:
        media_sequence["status"] = self.media_status

    # Start review session
    context.scene.review_session_active = True

    # Check if is reviewed
    set_reviewed_state(media_sequence)


class CompareProperties(bpy.types.PropertyGroup):
    """All compare properties"""

    compare_type: bpy.props.EnumProperty(
        items=[
            ("CROSS", "Default", "Displays the sequence and opacity can be modified"),
            (
                "DIFFERENCE",
                "Difference",
                "Subtracts the bottom layer from the top layer",
            ),
            ("MULTIPLY", "Multiply", "Multiply with below sequence"),
            ("WIPE", "Wipe", "Adjustable Wipe with below sequence"),
        ],
        name="Choose blend type",
        default="CROSS",
        update=compare_setup,
    )

    opacity: bpy.props.FloatProperty(
        name="Opacity",
        description="Change transparency",
        default=1,
        min=0,
        max=1,
        subtype="FACTOR",
        update=compare_refresh,
    )

    wipe_fader: bpy.props.FloatProperty(
        name="Wipe Fader",
        description="Position of Wipe split",
        default=0.5,
        min=0,
        max=1,
        subtype="FACTOR",
        update=compare_refresh,
    )

    wipe_angle: bpy.props.FloatProperty(
        name="Wipe Angle",
        description="Angle of Wipe split",
        default=-1.570796,  # Radians value for vertical split consistent with left->right slider
        min=-1.570796,
        max=1.570796,
        subtype="ANGLE",
        update=compare_refresh,
    )

    wipe_blur: bpy.props.FloatProperty(
        name="Wipe Blur",
        description="Blur smoothing of Wipe split",
        default=0,
        min=0,
        max=1,
        subtype="FACTOR",
        update=compare_refresh,
    )


def update_media_current_frame(self, context):
    """Update scene current frame when media current frame is set."""
    scene = context.scene
    media_sequence = get_media_sequence(scene.sequence_editor.active_strip)
    scene.frame_set(media_sequence.frame_start + self.media_current_frame)


classes = [CompareProperties]


def register():
    for cls in classes:
        bpy.utils.register_class(cls)

    bpy.types.Scene.all_annotations_displayed = bpy.props.BoolProperty(default=True)
    bpy.types.Scene.compare_properties = bpy.props.PointerProperty(
        type=CompareProperties
    )
    bpy.types.Scene.versions_displayed_sequence = bpy.props.StringProperty(
        description="Keep the name of the sequence the versions are displayed."
    )

    bpy.types.Scene.media_status = bpy.props.EnumProperty(
        items=available_statuses_callback,
        name="Media status",
        update=change_media_status,
    )

    bpy.types.Scene.media_current_frame = bpy.props.IntProperty(
        name="Current media frame",
        update=update_media_current_frame,
        min=0,
    )

    bpy.types.WindowManager.emphasize_drawings = bpy.props.FloatProperty(
        name="Emphasize Drawings",
        description="Increase the whiteness of the media to see the drawings better",
        default=0,
        min=0,
        max=100,
        subtype="PERCENTAGE",
        update=emphasize_drawings,
    )

    bpy.types.WindowManager.newer_to_older = bpy.props.BoolProperty(
        name="Newer to Older", description="Order notes from newer to older"
    )


def unregister():
    del bpy.types.WindowManager.emphasize_drawings
    del bpy.types.Scene.media_status
    del bpy.types.WindowManager.newer_to_older
    del bpy.types.Scene.versions_displayed_sequence
    del bpy.types.Scene.compare_properties
    del bpy.types.Scene.all_annotations_displayed

    for cls in reversed(classes):
        bpy.utils.unregister_class(cls)
